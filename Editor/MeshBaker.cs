﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MeshBaker
{
    public class MeshBaker : EditorWindow
    {
        private const int MAX_VERTEX_COUNT_PER_MESH = 65000;

        private static List<Vector3> _vertexes = new List<Vector3>();
        private static List<Vector4> _tangents = new List<Vector4>();
        private static List<Vector3> _normals = new List<Vector3>();
        private static List<int> _triangles = new List<int>();
        private static List<Vector2> _uvs = new List<Vector2>();

        private const string assetExt = ".asset";

        [MenuItem("GameObject/Bake Meshes", false, 0)]
        private static void GameObjectContext(MenuCommand menuCommand)
        {
            if (Selection.objects.Length == 0)
                return;

            if (Selection.objects.Length > 1)
                if (menuCommand.context != Selection.objects[0])
                    return;

            var path = MeshSaver.GetSavePath(Selection.objects[0].name + assetExt);

            if (string.IsNullOrEmpty(path)) return;

            Debug.Log($"{nameof(MeshBaker)} saved  mesh: {path}");
            MeshSaver.Save(WeldMesh(GetAllMeshesInGameObjectGroup(Selection.gameObjects)), path);
        }

        /// <summary> Collect all meshes in gameObjects. </summary>
        /// <param name="gameObjects">array of gameObjects</param>
        /// <returns>collected all meshes</returns>
        private static List<Mesh> GetAllMeshesInGameObjectGroup(GameObject[] gameObjects)
        {
            List<Mesh> meshes = new List<Mesh>();
            for (var i = 0; i < gameObjects.Length; i++)
                meshes.AddRange(GetAllMeshInGameObject(gameObjects[i]));

            return meshes;
        }

        private static List<Mesh> GetAllMeshInGameObject(GameObject gameObj)
        {
            List<Mesh> meshes = new List<Mesh>();

            MeshFilter[] meshFilters = gameObj.GetComponentsInChildren<MeshFilter>();
            for (int j = 0; j < meshFilters.Length; j++)
            {
                var meshFilter = meshFilters[j];
                var sharedMesh = meshFilter.sharedMesh;
                if (sharedMesh == null)
                {
                    Debug.LogError($"Missing mesh on a {meshFilter.gameObject.name}");
                    continue;
                }

                Mesh mesh = new Mesh
                {
                    vertices = sharedMesh.vertices,
                    triangles = sharedMesh.triangles,
                    uv = sharedMesh.uv,
                    normals = sharedMesh.normals,
                    colors = sharedMesh.colors,
                    tangents = sharedMesh.tangents
                };

                meshes.Add(TranslateMeshToWorldTransform(mesh, meshFilter.transform));
            }

            SkinnedMeshRenderer[] skinnedMeshRenderers =
                gameObj.GetComponentsInChildren<SkinnedMeshRenderer>();
            for (int k = 0; k < skinnedMeshRenderers.Length; k++)
            {
                var skinMesh = skinnedMeshRenderers[k];
                if (skinMesh == null)
                {
                    Debug.LogError($"Missing mesh on a {skinMesh.gameObject.name}");
                    continue;
                }

                Mesh mesh = BonesBaker.GetMeshFromSkinnedMeshRenderer(skinMesh);
                meshes.Add(TranslateMeshToWorldTransform(mesh, skinMesh.transform));
            }

            return meshes;
        }

        private static Mesh TranslateMeshToWorldTransform(Mesh mesh, Transform transform)
        {
            mesh.vertices = mesh.vertices.Select(transform.TransformPoint).ToArray();
            mesh.normals = mesh.normals.Select(transform.TransformDirection).ToArray();

            return mesh;
        }


        private static void CombineMeshesData(Mesh mesh)
        {
            if (_vertexes.Count + mesh.vertexCount > MAX_VERTEX_COUNT_PER_MESH)
            {
                Clear();
                Debug.LogError($"Unity doesn't support meshes > {MAX_VERTEX_COUNT_PER_MESH} vertices.");
                return;
            }

            int startCount = _vertexes.Count;

            for (int i = 0; i < mesh.vertices.Length; i++)
                _vertexes.Add(mesh.vertices[i]);

            for (int i = 0; i < mesh.triangles.Length; i++)
                _triangles.Add(mesh.triangles[i] + startCount);

            AddData(mesh.normals, _normals, mesh.vertices.Length);
            AddData(mesh.tangents, _tangents, mesh.tangents.Length);
            AddData(mesh.uv, _uvs, mesh.vertices.Length);

            void AddData<T>(T[] source, List<T> destination, int expectedCount)
            {
                // destination = new T [expectedCount];
                if (source.Length == 0)
                {
                    for (int i = 0; i < expectedCount; i++)
                        destination.Add(default);
                }
                else
                    destination.AddRange(source);
            }
        }

        private static Mesh WeldMesh(List<Mesh> meshes)
        {
            for (int i = 0; i < meshes.Count; i++)
                CombineMeshesData(meshes[i]);

            if (_vertexes.Count <= 0)
            {
                Debug.LogError($"Vertexes count = {_vertexes.Count}");
                return null;
            }

            Mesh mesh = new Mesh();

            mesh.SetVertices(_vertexes);
            mesh.SetTriangles(_triangles, 0);
            if (_normals.Count != 0)
                mesh.SetNormals(_normals);

            if (_tangents.Count != 0)
                mesh.SetTangents(_tangents);

            if (_uvs.Count != 0)
                mesh.SetUVs(0, _uvs);

            Clear();
            return mesh;
        }

        private static void Clear()
        {
            _vertexes.Clear();
            _normals.Clear();
            _tangents.Clear();
            _triangles.Clear();
            _uvs.Clear();
        }

        /// <summary> Bake singly bunch of meshes. </summary>
        /// <param name="arr">array of g-o with meshes.</param>
        /// <param name="path">Path for saving baked meshes</param>
        public static void BakeBunchOfMeshes<T>(T[] arr, string path = "Assets/") where T : Component
        {
            float count = arr.Length;
            float index = 0F;

            for (var i = 0; i < arr.Length; i++)
            {
                var go = arr[i].gameObject;
                index = i;
                EditorUtility.DisplayProgressBar($"Bake mesh {go.name} in progress", $"Baking... {index}/{count}",
                    index / count);

                MeshSaver.Save(WeldMesh(GetAllMeshInGameObject(go)), path + $"{go.name}" + assetExt);
            }

            EditorUtility.ClearProgressBar();
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        /// <summary> Bake meshes from single game object</summary>
        /// <param name="gameObj">game object with meshes</param>
        /// <param name="path">Path for saving baked meshes</param>
        public static void BakeMesh(GameObject gameObj, string path = "Assets/")
        {
            MeshSaver.Save(WeldMesh(GetAllMeshInGameObject(gameObj)), path + $"{gameObj.name}" + assetExt);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}