using UnityEditor;
using UnityEngine;


namespace MeshBaker
{
    public static class MeshSaver
    {
        public static string GetSavePath(string preferredName)
        {
            var path = EditorUtility.SaveFilePanel("Save Mesh Asset", "Assets/", preferredName, "asset");
            return FileUtil.GetProjectRelativePath(path);
        }

        public static void Save(Mesh mesh, string path)
        {
            if (mesh == null) return;
            // Debug.Log($"name: {mesh.name} path {path}");
            MeshUtility.Optimize(mesh);
            AssetDatabase.CreateAsset(mesh, path);
            // AssetDatabase.ImportAsset(path);
        }
    }
}