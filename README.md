# Editor mesh baker.
Simple context baking from hierarchy. Baker weld complex parent object with children to single mesh.

#### features:
- bake mesh from GameObject context menu.
- access to bake methods for custom scripting.
- support multiple baking.
- baker settings: Autosave path with enable/disable option.

## How to use:
#### Editor:
- Select in hierarchy 3D object open GameObject context menu, find `Bake mesh` > select a path to `Save`.

#### Scripting:

- `BakeMesh` - for baking single mesh based-on one gameObject.
- `BakeBunchOfMeshes` - bakes an a array of meshes.
